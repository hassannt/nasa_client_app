# NASA Demo app (Client)

# Technical requirements!
 To be able to set up the project, the following installations must be made:
  - Nodejs (latest)
  - yarn (latest)
 
### Running the project

Make sure to install all dependencies, run
```sh
$ yarn install
```
after installing all the dependencies you should be ready to run the server, run
```sh
$ yarn run dev
```
and the server should  be up and running.