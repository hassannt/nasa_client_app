import axios from 'axios'
export function authenticate(email, password) {
    return new Promise((reject, resolve) => {
        axios.get('/login', {
            params: {
                email,
                password
            }
        })
        .then(res => {
            return resolve(res.data.token)
        })
        .catch(e => {
            return reject(res.data.errors)
        })
    })
}