
import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from './views/Login.vue'
import Homepage from './views/HomePage.vue'
import Image from './views/Image.vue'
import Favs from './views/MyFavs.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/', name: 'home', component: Homepage },
    { path: '/login', component: Login },
    { path: '/images/:id', component: Image },
    { path: '/me/favourites', component: Favs }
]

const router =  new VueRouter({
    mode: 'history',
    routes
})
router.beforeEach((to, from, next) => {
    const publicPages = ['/login', '/register'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user_token');
  
    if (authRequired && !loggedIn) {
      return next('/login');
    }
    next();
  })

export default router
