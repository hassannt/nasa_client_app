import Vue from 'vue'
import App from './App.vue'

import routes from './routes'
import store from './store'

new Vue({
  el: '#app',
  store,
  router: routes,
  render: h => h(App)
})
