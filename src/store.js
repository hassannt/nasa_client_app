import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const FAVOURITES_URL = 'http://localhost:3000/me/favourites'
const SEARCH_URL = 'http://localhost:3000/search'
const MAX_ITEMS = 10

export default new Vuex.Store({
    state: {
        userFavourites: [],
        userFavouritesIDs: [],
        totalSearchItems: 0,
        searchItems: []
    },
    mutations: {
        SET_FIELD(state, data) {
            state[data.fieldName] = data.value
        }
    },
    actions: {
        search({ commit, state }, searchKey) {
            const token = localStorage.getItem("user_token")
            const url = `${SEARCH_URL}?query=${encodeURIComponent(searchKey)}&start_index=${state.searchItems.length}&max_items=${MAX_ITEMS}`
            fetch(url, {
                mode: 'cors',
                headers: new Headers({
                    'Authorization': `Bearer ${token}`
                })
            })
                .then(res => {
                    res.json().then(jres => {
                        commit("SET_FIELD", {
                            fieldName: 'searchItems',
                            value: jres.data
                        })
                    })
                })
                .catch(console.log)
        },
        getUserFavourites({ commit, state }) {
            const token = localStorage.getItem("user_token")
            fetch(FAVOURITES_URL, {
                mode: 'cors',
                headers: new Headers({
                    'Authorization': `Bearer ${token}`
                })

            })
                .then(res =>
                    res.json().then(jres => {
                        state.userFavouritesIDs = jres.data.map(favourite => favourite.id)
                        state.userFavourites = jres.data
                    }))
        },
        updateUserFavourites({ commit, state }) {
            const token = localStorage.getItem("user_token")
            fetch(FAVOURITES_URL, {
                method: 'PATCH',
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    favourites: state.userFavouritesIDs
                })
            })
                .then(res =>
                    res.json().then(jres => {
                        commit("SET_FIELD", {
                            fieldName: 'userFavourites',
                            value: jres.data
                        })
                        commit("SET_FIELD", {
                            fieldName: 'userFavouritesIDs',
                            value: jres.data.map(favourite => favourite.id)
                        }) 
                    }))
        }
    }
})